package org.nrg.xnat.fornixcore;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "fornix_plugin_core", name = "XNAT 1.7 FORNIX Core Plugin", description = "This is the XNAT 1.7 FORNIX Core Plugin.",
        dataModels = {
                @XnatDataModel(value = "visit:visitData",
                        singular = "Visit",
                        plural = "Visits"),
				@XnatDataModel(value = "cnda:manualVolumetryData",
                        singular = "Manual Volumetry",
                        plural = "Manual Volumetries"),
				@XnatDataModel(value = "cnda:radiologyReadData",
                        singular = "Radiology Read",
                        plural = "Radiology Reads"),
				@XnatDataModel(value = "cnda:modifiedScheltensData",
                        singular = "Mod Scheltens",
                        plural = "Mod Scheltens"),
				@XnatDataModel(value = "cnda:atlasScalingFactorData",
                        singular = "ASF",
                        plural = "ASFs"),
				@XnatDataModel(value = "cnda:clinicalAssessmentData",
                        singular = "CLIN",
                        plural = "CLINs"),
				@XnatDataModel(value = "cnda:psychometricsData",
                        singular = "PSYCH",
                        plural = "PSYCHs"),
				@XnatDataModel(value = "cnda:csfData",
                        singular = "CSF",
                        plural = "CSFs"),
				@XnatDataModel(value = "cnda:dtiData",
                        singular = "DTI",
                        plural = "DTIs"),
				@XnatDataModel(value = "cnda:segmentationFastData",
                        singular = "Fast Seg",
                        plural = "Fast Segs"),
				@XnatDataModel(value = "cnda:petTimeCourseData",
                        singular = "Manual PET Timecourse",
                        plural = "Manual PET Timecourse"),
				@XnatDataModel(value = "cnda:vitalsData",
                        singular = "Vitals Data",
                        plural = "Vitals Data")
						})
public class CorePlugin {
}
